package ie.wit.medRecord.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import ie.wit.medRecord.models.Doctor;
import ie.wit.medRecord.models.Patient;

import static android.R.attr.id;

/**
 * Created by Matt_ on 09/03/2017.
 */

public class DBManager {
    private SQLiteDatabase database;
    private DBDesigner dbhelper;

    public DBManager(Context context) {dbhelper = new DBDesigner( context );}

    public void open() throws SQLException {
        database = dbhelper.getWritableDatabase();
    }

    private static final String[] allColumns = {
            DBDesigner.COLUMN_ID,
            DBDesigner.COLUMN_NAME,
            DBDesigner.COLUMN_GENDER,
            DBDesigner.COLUMN_AGE,
            DBDesigner.COLUMN_ADDRESS,
            DBDesigner.COLUMN_ILLNESS,
            DBDesigner.COLUMN_MEDICATION,
    };

    public void close() {database.close();}

   /* public void add(Patient patient) {
        ContentValues values = new ContentValues();
        values.put("name", patient.getName());
        values.put("gender", patient.getGender());
        values.put("age", patient.getAge());
        values.put("address", patient.getAddress());
        values.put("illness", patient.getIllness());
        values.put("medication", patient.getMedication());
        long insertid = database.insert(DBDesigner.TABLE_PATIENTS,null,values);
        patient.setId(insertid);

        database.insert("patients", null, values);
    }*/


    ///////////new add patient method////////////////
    public Patient add(Patient Patient){
        ContentValues values  = new ContentValues();
        values.put(DBDesigner.COLUMN_NAME,Patient.getName());
        values.put(DBDesigner.COLUMN_GENDER, Patient.getGender());
        values.put(DBDesigner.COLUMN_AGE, Patient.getAge());
        values.put(DBDesigner.COLUMN_ADDRESS, Patient.getAddress());
        values.put(DBDesigner.COLUMN_ILLNESS, Patient.getIllness());
        values.put(DBDesigner.COLUMN_MEDICATION, Patient.getMedication());
        long insertid = database.insert(DBDesigner.TABLE_PATIENTS,null,values);
        Patient.setId(insertid);
        return Patient;

    }

    /*public List<Patient> getAll(){
        List<Patient> patientList = new ArrayList<>(  );
        Cursor cursor = database.rawQuery("SELECT * FROM patients", null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Patient patient = toPatient(cursor);
            patientList.add(patient);
            cursor.moveToNext();
        }
        cursor.close();
        return patientList;
    }*/

    ///////////new list patient method/////////////////
    public List<Patient> getAll() {

        Cursor cursor = database.query(DBDesigner.TABLE_PATIENTS,allColumns,null,null,null, null, null);

        List<Patient> patients = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                Patient allpatients = new Patient();
                allpatients.setId(cursor.getLong(cursor.getColumnIndex(DBDesigner.COLUMN_ID)));
                allpatients.setName(cursor.getString(cursor.getColumnIndex(DBDesigner.COLUMN_NAME)));
                allpatients.setGender(cursor.getString(cursor.getColumnIndex(DBDesigner.COLUMN_GENDER)));
                allpatients.setAge(cursor.getInt(cursor.getColumnIndex(DBDesigner.COLUMN_AGE)));
                allpatients.setAddress(cursor.getString(cursor.getColumnIndex(DBDesigner.COLUMN_ADDRESS)));
                allpatients.setIllness(cursor.getString(cursor.getColumnIndex(DBDesigner.COLUMN_ILLNESS)));
                allpatients.setMedication(cursor.getString(cursor.getColumnIndex(DBDesigner.COLUMN_MEDICATION)));
                patients.add(allpatients);
            }
        }
        // return All Employees
        return patients;
    }

   /////// //////////////////////Get a single Employee
    public Patient getOnePatient(long id) {
        Cursor cursor =
                database.query(DBDesigner.TABLE_PATIENTS, allColumns, DBDesigner.COLUMN_ID + "=?", new String[] {
                        String.valueOf(id)}, null, null, null, null);
        if (cursor !=null)
            cursor.moveToFirst();
        Patient p = new Patient(Long.parseLong(cursor.getString(0)), cursor.getString(1), cursor.getString(2),
                cursor.getInt(3),cursor.getString(4), cursor.getString(5), cursor.getString(6));
        //return Employee
        return p;
    }

   /* public void delete_patient(String name) {
        dbhelper.getWritableDatabase().delete("patients", "name ='" + name + "'", null);
    }*/

   ///////new delete patient function///////////////
   public void delete_patient(EditText patient) {

       database.delete(DBDesigner.TABLE_PATIENTS, DBDesigner.COLUMN_ID + "=" + patient.getId(), null);
   }



    /*public void update_patient(String old_name, String new_name) {
        dbhelper.getWritableDatabase().execSQL( "UPDATE patients SET name ='" + new_name + "' WHERE name ='" + old_name+ "'" );
    }*/

    //////new edit patient method////////////
    // Updating Employee
    public int update_patient(Patient patient) {

        ContentValues values = new ContentValues();
        values.put(DBDesigner.COLUMN_NAME, patient.getName());
        values.put(DBDesigner.COLUMN_GENDER, patient.getGender());
        values.put(DBDesigner.COLUMN_AGE, patient.getAge());
        values.put(DBDesigner.COLUMN_ADDRESS, patient.getAddress());
        values.put(DBDesigner.COLUMN_ILLNESS, patient.getIllness());
        values.put(DBDesigner.COLUMN_MEDICATION, patient.getMedication());

        // updating row
        return database.update(DBDesigner.TABLE_PATIENTS, values,
                DBDesigner.COLUMN_ID + "=?",new String[] { String.valueOf(patient.getId())});
    }

    public int getId() {
        return id;
    }
   /* public void delete(Patient patient) {

        database.delete(dbhelper.DATABASE_CREATE_TABLE_PATIENTS , dbhelper.COLUMN_ID + "=" + patient.getEmpId(), null);
    }*/



   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////DOCTORS////////////////////////////////////////////////////////////////////////////////////////////////////


    public void addDoc(Doctor doctor) {
        ContentValues values = new ContentValues();
        values.put("name", doctor.getName());
        values.put("gender", doctor.getGender());
        values.put("age", doctor.getAge());
        values.put("field", doctor.getField());
        values.put("lengthservice", doctor.getLengthService());



        database.insert("doctors", null, values);
    }

    public List<Doctor> getAllDoc(){
        List<Doctor> doctorList = new ArrayList<>(  );
        Cursor cursor = database.rawQuery("SELECT * FROM doctors", null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Doctor doctor = toDoctor(cursor);
            doctorList.add(doctor);
            cursor.moveToNext();
        }
        cursor.close();
        return doctorList;
    }

    public void delete_doctor(String name) {
        dbhelper.getWritableDatabase().delete("doctors", "name ='" + name + "'", null);
    }


    public void update_doctor(String old_name, String new_name) {
        dbhelper.getWritableDatabase().execSQL( "UPDATE doctors SET name ='" + new_name + "' WHERE name ='" + old_name + "'" );
    }





    private Patient toPatient(Cursor cursor) {
        Patient pojo = new Patient();
       // pojo.id = cursor.getInt( 0 );
        pojo.name = cursor.getString( 1 );
        pojo.gender = cursor.getString(2);
        pojo.age = cursor.getInt(3);
        pojo.address = cursor.getString( 4 );
        pojo.illness = cursor.getString( 5 );
        pojo.medication = cursor.getString( 6 );
        return pojo;
    }

    private Doctor toDoctor(Cursor cursor) {
        Doctor pojod = new Doctor();
        // pojo.id = cursor.getInt( 0 );
        pojod.name = cursor.getString( 1 );
        pojod.gender = cursor.getString(2);
        pojod.age = cursor.getInt(3);
        pojod.field = cursor.getString( 4 );
        pojod.lengthService = cursor.getInt( 5 );
        return pojod;
    }



    public void reset() {
        database.delete( "patients", null, null );
        database.delete( "doctors", null, null);

    }
}
